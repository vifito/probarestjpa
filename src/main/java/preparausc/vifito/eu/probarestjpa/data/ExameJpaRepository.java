package preparausc.vifito.eu.probarestjpa.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ExameJpaRepository extends JpaRepository<Pregunta, Long> {    
}
