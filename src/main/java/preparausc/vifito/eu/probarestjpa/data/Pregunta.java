package preparausc.vifito.eu.probarestjpa.data;

import jakarta.persistence.Table;

import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "preguntas")
@Builder
@NoArgsConstructor @AllArgsConstructor
@Getter @Setter
public class Pregunta {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long ID;

    @Column(length = 1024)
    private String texto;

    @Column(length = 1024)
    private String explicacion;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Resposta> respostas;

}
