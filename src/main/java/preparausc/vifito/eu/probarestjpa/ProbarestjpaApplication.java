package preparausc.vifito.eu.probarestjpa;

import java.util.ArrayList;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import preparausc.vifito.eu.probarestjpa.data.ExameRepository;
import preparausc.vifito.eu.probarestjpa.data.Pregunta;
import preparausc.vifito.eu.probarestjpa.data.Resposta;

@SpringBootApplication
public class ProbarestjpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProbarestjpaApplication.class, args);
	}

/* 	@Bean
	public CommandLineRunner cargarDatos(ExameRepository exameRepository) {
		return (args) -> {
			Pregunta pregunta1 = Pregunta.builder()
				.texto("Indique la opción correcta en relación a la virtualización basada en contenedores:")
				.explicacion("Tema 12. Ver definición y características de Docker (https://nubeusc.sharepoint.com/:b:/s/ostresdesempre/EWJsOxEDRr1PlnPJeXD9lH4BsVhB64nBRfJs-jpSX6ae7Q?e=KCTNI5).")
				.build();
			
			ArrayList<Resposta> respostas = new ArrayList<Resposta>();
			respostas.add(
				Resposta.builder()
					.texto("Los contenedores son instancias aisladas de ejecución de usuario que comparten un núcleo de sistema operativo común.")
					.correcta(true)
					.build()
			);
			respostas.add(
				Resposta.builder()
					.texto("Cada contenedor integra un sistema operativo completo, incluido el kernel.")
					.correcta(false)
					.build()
			);
			respostas.add(
				Resposta.builder()
					.texto("Los contenedores requieren ejecutarse dentro de un hipervisor.")
					.correcta(false)
					.build()
			);
			respostas.add(
				Resposta.builder()
					.texto("Los contenedores no pueden ejecutarse dentro de máquinas virtuales.")
					.correcta(false)
					.build()
			);		
			pregunta1.setRespostas(respostas);
			
			exameRepository.save(pregunta1);
		};
	} */
}
