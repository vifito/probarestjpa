package preparausc.vifito.eu.probarestjpa.data;

import org.springframework.data.repository.CrudRepository;

public interface ExameRepository extends CrudRepository<Pregunta, Long> {    
}
