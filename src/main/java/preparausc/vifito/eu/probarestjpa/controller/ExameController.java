package preparausc.vifito.eu.probarestjpa.controller;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import preparausc.vifito.eu.probarestjpa.data.ExameRepository;
import preparausc.vifito.eu.probarestjpa.data.Pregunta;

@RestController
@RequestMapping("/exame")
public class ExameController {
    @Autowired
    private ExameRepository exameRepository;
    
    @GetMapping(path = "/preguntas")
    public Iterable<Pregunta> list() {
        return exameRepository.findAll();
    }

    @PostMapping(path = "/preguntas")
    public ResponseEntity<Object> create(@RequestBody Pregunta pregunta) {
        Pregunta preguntaGardada = exameRepository.save(pregunta);

        // Montar a URL que se pode empregar por GET
	    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(preguntaGardada.getID()).toUri();

	    return ResponseEntity.created(location).build();
    }

    @GetMapping(path = "/preguntas/{id}", produces = "application/json")
    public Pregunta read(@PathVariable("id") Long id) {
        
        Optional<Pregunta> pregunta = exameRepository.findById(id);

	    if (pregunta.isEmpty())
		    throw new PreguntaNotFoundException(id);

        return pregunta.get();
    }

    @PutMapping("/preguntas/{id}")
    public ResponseEntity<Object> updateStudent(@RequestBody Pregunta pregunta, @PathVariable Long id) {

	    Optional<Pregunta> preguntaOptional = exameRepository.findById(id);

	    if (preguntaOptional.isEmpty())
		    return ResponseEntity.notFound().build();

	    pregunta.setID(id);
        exameRepository.save(pregunta);

	    return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/preguntas/{id}")
    public void deleteStudent(@PathVariable long id) {
        exameRepository.deleteById(id);
    }

}
