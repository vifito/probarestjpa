package preparausc.vifito.eu.probarestjpa.controller;

public class PreguntaNotFoundException extends RuntimeException {
    private Long id;
    
    public PreguntaNotFoundException(Long id) {
        this.id = id;
    }

    public String toString() {
        return String.format("Pregunta con id=%d NON se atopa", id);
    }
}
