INSERT INTO PREGUNTAS (ID, TEXTO, EXPLICACION) VALUES (1, 'Indique la opción correcta en relación a la virtualización basada en contenedores:', 'Ver definición y características de Docker (https://nubeusc.sharepoint.com/:b:/s/ostresdesempre/EWJsOxEDRr1PlnPJeXD9lH4BsVhB64nBRfJs-jpSX6ae7Q?e=KCTNI5).');

INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (1, 'Los contenedores son instancias aisladas de ejecución de usuario que comparten un núcleo de sistema operativo común.', TRUE);
INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (2, 'Cada contenedor integra un sistema operativo completo, incluido el kernel.', FALSE);
INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (3, 'Los contenedores requieren ejecutarse dentro de un hipervisor.', FALSE);
INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (4, 'Los contenedores no pueden ejecutarse dentro de máquinas virtuales.', FALSE);

INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (1, 1);
INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (1, 2);
INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (1, 3);
INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (1, 4);

--

INSERT INTO PREGUNTAS (ID, TEXTO, EXPLICACION) VALUES (2, 'En relación a los tipos de servicios que son objeto de la API de Kubernetes en los que se describe cómo se accede a las aplicaciones. ¿De qué tipo de servicio es el que se usa una dirección interna del clúster y a la que solo se accede dentro del mismo?', 'Conceptos importantes. https://kubernetes.io/docs/concepts/services-networking/service/ . LoadBalancer expone el servicio con un LB exterior ( por ejemplo de la nube). Nodeport expone el servicio en caada nodo con una IP estática y se puede acceder desde fuera. ExternalName mapea el service al contenido del campo externalName al devolver un registro CNAME con su valor. No se configura ningún tipo de proxy. A parte tambien se puede usar Ingress para exponer tu servicio. Ingress no es un tipo de Services pare actúa como punto de entrada del clúster.');

INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (5, 'LoadBalancer', FALSE);
INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (6, 'ClusterIP', TRUE);
INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (7, 'NodePort', FALSE);
INSERT INTO RESPOSTAS (ID, TEXTO, CORRECTA) VALUES (8, 'ExternalName', FALSE);

INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (2, 5);
INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (2, 6);
INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (2, 7);
INSERT INTO PREGUNTAS_RESPOSTAS (PREGUNTA_ID, RESPOSTAS_ID) VALUES (2, 8);

--

